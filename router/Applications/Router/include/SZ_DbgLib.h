/**************************************************************************//**
  \file  SZ_DbgLib.h

  \brief The header file describes the debug function proto and queue size.

  \author
    Concept2 Silicon Systems Pvt Ltd \n

  \internal
  History:
    17/12/14 Prateek Jindal - Created
 ******************************************************************************/
#ifndef _SZ_DBG_LIB_H
#define _SZ_DBG_LIB_H

/******************************************************************************
                   Includes section
******************************************************************************/
#include <usart.h>
#include <stdarg.h>

#define DBG_MAX_MSG_SIZE			200
#define DBG_BUFFER_MAX_SIZE			1024

/***********************************************************************************
  Init USART, register USART callbacks.

  Parameters:
    none

  Return:
    none

 ***********************************************************************************/
void DBG_Init(void);

/***********************************************************************************
  New message being sent into USART has to be put into queue.

  Parameters:
    newMessage - new message fields.

  Return:
    none

 ***********************************************************************************/
int8_t DBG_Print(char *format,...);

#endif