/**************************************************************************//**
	\file	SZ_DbgLib.c

	\brief Implementation of Debug library.

	\author
		Concept2 Silicon Systems Pvt Ltd \n

	\internal
	History:
		17/12/14 Prateek Jindal - Created
 ******************************************************************************/
/******************************************************************************
									 Includes section
******************************************************************************/
#include <stdio.h>
#include <string.h>
#include <SZ_DbgLib.h>

#define DEBUG_MODE  

/*****************************************************************************
								Definitions section
******************************************************************************/
bool isSending;

#ifdef DEBUG_MODE
	uint16_t head,tail,newHead;
	HAL_UsartDescriptor_t debugUsartDesc;
	uint8_t dbgBuffer[DBG_BUFFER_MAX_SIZE],tempBuffer[DBG_MAX_MSG_SIZE];
	uint8_t rxBuffer[1];
#endif

/*****************************************************************************
									Types section
******************************************************************************/
#ifdef DEBUG_MODE
	static void readConfByteEvent(uint16_t readBytesLen);
	static void writeDebugConfirm(void);

/***********************************************************************************
	Send next message from queue.

	Parameters:		none

	Return:		none
 ***********************************************************************************/
static void sendNextMessage(void)
{
	if(tail== newHead)
	{
		tail= head= newHead= 0;
		isSending= false;
	}
	else if(newHead < tail)
	{
		head= 0;
		HAL_WriteUsart(&debugUsartDesc,&dbgBuffer[tail],DBG_BUFFER_MAX_SIZE - tail);
	}
	else
	{
		head= newHead;
		HAL_WriteUsart(&debugUsartDesc,&dbgBuffer[tail],head - tail);
	}
}

/***********************************************************************************
	Writing confirmation has been received. New message can be sent.

	Parameters:		none

	Return:		none
 ***********************************************************************************/
static void writeDebugConfirm(void)
{
	tail= head;

	//send next message
	sendNextMessage();
}

/******************************************************************************
	\brief New USART bytes were received.

	\param[in] readBytesLen - count of received bytes.

	\return None.
******************************************************************************/
static void readConfByteEvent(uint16_t bytesReceived)
{
	bytesReceived--;
}
#endif

/***********************************************************************************
	Init USART, register USART callbacks.

	Parameters:		none

	Return:		none
 ***********************************************************************************/
void DBG_Init()
{
	isSending= false;

#ifdef DEBUG_MODE
	debugUsartDesc.tty= USART_CHANNEL_0;
	debugUsartDesc.mode= USART_MODE_ASYNC;
	debugUsartDesc.flowControl= USART_FLOW_CONTROL_NONE;
	debugUsartDesc.baudrate= USART_BAUDRATE_115200;
	debugUsartDesc.dataLength= USART_DATA8;
	debugUsartDesc.parity= USART_PARITY_NONE;
	debugUsartDesc.stopbits= USART_STOPBIT_1;
	debugUsartDesc.rxBuffer= rxBuffer;
	debugUsartDesc.rxBufferLength= 1;
	debugUsartDesc.txBuffer= NULL;
	debugUsartDesc.txBufferLength= 0;
	debugUsartDesc.rxCallback= readConfByteEvent;
	debugUsartDesc.txCallback= writeDebugConfirm;

	head= newHead= tail= 0;
	HAL_OpenUsart(&debugUsartDesc);
#endif
}

/***********************************************************************************
	New message being sent into USART has to be put into queue.

	Parameters:
		newMessage - new message fields.

	Return:
		none

 ***********************************************************************************/
int8_t DBG_Print(char *format,...)
{
#ifdef DEBUG_MODE
	uint16_t size;
	va_list var;

	va_start(var,format);
	size= vsprintf((char *)tempBuffer,format,var);
	va_end(var);

	if((tail== newHead) && (newHead== head))
	{
		memcpy(dbgBuffer,tempBuffer,size);
		newHead= size;
		tail= 0;
	}
	else if(newHead < tail)
	{
		if((newHead - tail + size) < DBG_BUFFER_MAX_SIZE)
		{
			memcpy(&dbgBuffer[newHead],tempBuffer,size);
			newHead += size;
		}
		else
			return -1;
	}
	else
	{
		if((newHead + size) < DBG_BUFFER_MAX_SIZE)
		{
			memcpy(&dbgBuffer[newHead],tempBuffer,size);
			newHead += size;
		}
		else if((newHead - tail + size) < DBG_BUFFER_MAX_SIZE)
		{
			uint16_t len= DBG_BUFFER_MAX_SIZE - newHead;
			memcpy(&dbgBuffer[newHead],tempBuffer,len);
			memcpy(&dbgBuffer,&tempBuffer[len],size - len);
			newHead += size - DBG_BUFFER_MAX_SIZE;
		}
		else
			return -1;
	}

	if(isSending== false)
	{
		isSending= true;
		sendNextMessage();
	}
#else
	isSending = false;
	(void *)(format);
#endif
	return 0;
}
