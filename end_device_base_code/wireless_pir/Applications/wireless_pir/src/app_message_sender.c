#include <sysUtils.h>
#include <sysQueue.h>
#include <app_apl_task.h>
#include <sysTaskManager.h>
#include <zdo.h>
#include <configServer.h>
#include <aps.h>
#include <mac.h>
#include <app_command_handler.h>
#include <app_message_sender.h>
#include <app_end_device.h>
#include <bspUid.h>
#include <usart.h>
#include <debug.h>
#include <converted_library_types.h>

typedef enum
{
  APP_MSG_TX_FREE_STATE,
  APP_MSG_TX_WAITING_STATE,
  APP_MSG_TX_SENDING_DONE_STATE,
  APP_MSG_TX_SENDING_FAILED_STATE
} app_message_trasmission_state_t;

typedef struct _Apptx_queue_element_t
{
  queue_element_t next;
  app_command_handler_tranmission_frame_t message;
  void (*onTxFinished)(app_command_handler_tranmission_frame_t *tramsission_frame);
    
} app_tx_queue_element_t;

static void message_sender_aps_data_conf(APS_DataConf_t *confInfo);
static queue_element_t *app_remove_queue_element(queue_descriptor_t *queue, queue_element_t *element);
static app_command_handler_tranmission_frame_t *app_get_first_tramsission_frame(queue_descriptor_t *queue);

static app_message_trasmission_state_t tx_state = APP_MSG_TX_FREE_STATE;
static uint8_t failed_transmission = 0;

static app_tx_queue_element_t  app_tramsission_buffer[MAX_APP_MSG_QUEUE_COUNT];
static DECLARE_QUEUE(app_to_send_queue);
static DECLARE_QUEUE(app_free_queue);
static DECLARE_QUEUE(app_sent_queue);
static DECLARE_QUEUE(app_done_queue);

extern uint8_t send_packet;
extern uint32_t packet_count;
extern app_network_command_payload_t app_network_info;
uint32_t pkt_success = 0;
uint32_t pkt_failure = 0;


void app_init_message_sender(void)
{
  tx_state = APP_MSG_TX_FREE_STATE;

  reset_queue(&app_to_send_queue);
  reset_queue(&app_free_queue);
  reset_queue(&app_sent_queue);
  reset_queue(&app_done_queue);

  for (uint8_t i = 0; i < ARRAY_SIZE(app_tramsission_buffer); i++)
  {
    if(!put_element_into_queue(&app_free_queue, &app_tramsission_buffer[i].next))
    {
      /* failed to queue */
    }
  }
}

void app_message_sender(void)
{
  app_command_handler_tranmission_frame_t *tramsission_frame;

  switch (tx_state)
  {
		 case APP_MSG_TX_FREE_STATE:

			  tramsission_frame = app_get_first_tramsission_frame(&app_to_send_queue);

			if (tramsission_frame)
			{
			  tramsission_frame->msgParams.APS_DataConf = message_sender_aps_data_conf;
			  tramsission_frame->msgParams.asdu = (uint8_t *)(&tramsission_frame->command_frame.in_command);
			  aps_data_request(&tramsission_frame->msgParams);

			  if(!put_element_into_queue(&app_sent_queue, delete_head_element_in_queue(&app_to_send_queue)))
			  {
				 /* failed to queue */
			  }

			  app_post_message_sender_task();
			}
			break;

		 case APP_MSG_TX_SENDING_FAILED_STATE:   
			  #if APP_DEVICE_TYPE != DEV_TYPE_COORDINATOR
				  app_leave_network();
			  #endif

		 case APP_MSG_TX_SENDING_DONE_STATE:
			tramsission_frame = app_get_first_tramsission_frame(&app_done_queue);

			while (tramsission_frame)
			{
			  app_tx_queue_element_t *tx_queue_element = GET_STRUCT_BY_FIELD_POINTER(app_tx_queue_element_t, 
																									  message, 
																									  tramsission_frame);

			  if (tx_queue_element->onTxFinished)
			  {
				 tx_queue_element->onTxFinished(&tx_queue_element->message);
			  }

			  if(!put_element_into_queue(&app_free_queue, app_remove_queue_element(&app_done_queue, &tx_queue_element->next)))
			  {
				 /* failed to queue */
			  }
			  tramsission_frame = app_get_first_tramsission_frame(&app_done_queue);
			}

			tx_state = APP_MSG_TX_FREE_STATE;
			app_post_message_sender_task();
			break;

		 default:
		 break;
    }
}

static void message_sender_aps_data_conf(APS_DataConf_t *confInfo)
{
    queue_descriptor_t *destination_queue;
    app_tx_queue_element_t *tx_queue_element = GET_STRUCT_BY_FIELD_POINTER(app_tx_queue_element_t,
                                                                    message.msgParams.confirm,
                                                                    confInfo);

    if (APS_SUCCESS_STATUS == confInfo->status)
    {
		if ( pkt_success != packet_count)
		{
			pkt_success = packet_count;
			 debug_print("PacketCount::%d Success \r\n",packet_count);
			 	  send_packet = 1;
		}

      failed_transmission = 0;
      tx_state = APP_MSG_TX_SENDING_DONE_STATE;
      destination_queue = &app_done_queue;
    }
  else
  {
	 if ( pkt_failure != packet_count)
	 {
		 pkt_failure = packet_count;
		debug_print("LQI::%d, RSSI :%d, PacketCount::%d failed\r\n",app_network_info.lqi, app_network_info.rssi ,packet_count);
	 }

    if (APP_THRESHOLD_FAILED_TRANSMISSION > ++failed_transmission)
    {
      tx_state = APP_MSG_TX_FREE_STATE;
      destination_queue = &app_to_send_queue;
    }
    else
    {
      failed_transmission = 0;
      tx_state = APP_MSG_TX_SENDING_FAILED_STATE;
      destination_queue = &app_done_queue;
    }
  }

  if(!put_element_into_queue(destination_queue, app_remove_queue_element(&app_sent_queue, &tx_queue_element->next)))
  {
    /* failed to queue */
  }
  app_post_message_sender_task();
}

static queue_element_t *app_remove_queue_element(queue_descriptor_t *queue, queue_element_t *element)
{
  if (queue->head == element)
  {
    return delete_head_element_in_queue(queue);
  }
  else
  {
    queue_element_t *prev = queue->head;

    while (prev->next)
    {
      if (prev->next == element)
      {
        prev->next = element->next;
        break;
      }
      prev = prev->next;
    }
  }

  return element;
}

bool is_tx_queue_free(void)
{
    return (get_queue_element(&app_to_send_queue) || 
          get_queue_element(&app_sent_queue) ||
          get_queue_element(&app_done_queue)) ? false : true;
}

static app_command_handler_tranmission_frame_t *app_get_first_tramsission_frame(queue_descriptor_t *queue)
{
  queue_element_t *queueElement = get_queue_element(queue);

  if (queueElement)
  {
    return &(GET_STRUCT_BY_FIELD_POINTER(app_tx_queue_element_t,
                                         next,
                                         queueElement))->message;
  }

  return NULL;
}

bool app_create_transmission_frame(APS_DataReq_t **pMsgParamsPtr, app_command_type **command_pointer, void (*onTxFinishedCb)(app_command_handler_tranmission_frame_t *tramsission_frame))
{
  app_tx_queue_element_t *apptx_queue_element = NULL;
  queue_element_t *free_element = NULL;
  bool result;

  sys_assert((command_pointer && pMsgParamsPtr), TXQUEUEINSERTASSERT_0);

  free_element = get_queue_element(&app_free_queue);

  if (free_element)
  {
    if(!put_element_into_queue(&app_to_send_queue, delete_head_element_in_queue(&app_free_queue)))
    {
      /* failed to queue */
    }
    apptx_queue_element = GET_STRUCT_BY_FIELD_POINTER(app_tx_queue_element_t,
                                                    next,
                                                    free_element);

    if (*command_pointer)
    {
      memcpy(&apptx_queue_element->message.command_frame.in_command, *command_pointer, sizeof(app_command_type));
    }

    if (*pMsgParamsPtr)
    {
      memcpy(&apptx_queue_element->message.msgParams, *pMsgParamsPtr, sizeof(APS_DataReq_t));
    }

    *command_pointer = &apptx_queue_element->message.command_frame.in_command;
    *pMsgParamsPtr = &apptx_queue_element->message.msgParams;
    apptx_queue_element->onTxFinished = onTxFinishedCb;

    result = true;
  }
  else
  {
    result = false;
  }

  app_post_message_sender_task();
  return result;
}

