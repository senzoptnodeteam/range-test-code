#include <sysUtils.h>
#include <sysQueue.h>
#include <app_apl_task.h>
#include <sysTaskManager.h>
#include <zdo.h>
#include <configServer.h>
#include <aps.h>
#include <mac.h>
#include <app_command_handler.h>
#include <app_message_sender.h>
#include <app_end_device.h>
#include <bspUid.h>
#include <usart.h>
#include <debug.h>
#include <converted_library_types.h>

#if APP_USE_OTAU == 1
#include <app_zcl_manager.h>
#endif

#if defined(_ENABLE_PERSISTENT_SERVER_) && (APP_USE_PDS == 1)
#include <pdsDataServer.h>
#include <resetReason.h>
#endif

#ifdef ZAPPSI_HOST
#include <zsiNotify.h>
#endif

#define START_NETWORK_BLINKING_INTERVAL 500
#define TIMER_WAKEUP 50

typedef enum
{
	START_BLINKING,
	STOP_BLINKING
} blinking_action_type;

void APL_TaskHandler(void);
void zdo_network_update_notification_handler(zdo_network_update_notification_t *network_params);
void zdo_wakeup_indication(void);

app_network_command_payload_t  app_network_info =
{
	.softVersion     = CCPU_TO_LE32(0x01010100),
	.channelMask     = CCPU_TO_LE32(CS_CHANNEL_MASK),
	.boardInfo = {
		.boardType    = MESHBEAN_SENSORS_TYPE,
		.sensorsSize  = sizeof(app_network_info.boardInfo.meshbean),
	},
	#if APP_USE_DEVICE_CAPTION == 1
	.deviceCaption = {
		.fieldType = DEVICE_CAPTION_FIELD_TYPE,
		.size      = APP_DEVICE_CAPTION_SIZE,
		.caption   = APP_DEVICE_CAPTION
	}
	#endif
};

static ZDO_ZdpReq_t leave_request;
static ZDO_StartNetworkReq_t zdo_start_request;
static get_devicetype_interface_t device_interface;
static SimpleDescriptor_t simpleDescriptor;
static APS_RegisterEndpointReq_t end_point_params;
static app_state_type app_state = APP_INITING_STATE;

static struct
{
	uint8_t app_sub_task_posted  : 1;
	uint8_t app_command_handler_task_posted  : 1;
	uint8_t app_message_sender_task_posted  : 1;
} app_task_flags =
{
	.app_sub_task_posted             = false,
	.app_command_handler_task_posted = false,
	.app_message_sender_task_posted  = false
};


#if defined(_ENABLE_PERSISTENT_SERVER_) && (APP_USE_PDS == 1)
static bool restored_from_persistent_data_storage;
#endif

static void app_post_global_task(void);
static void app_initialization(void);
static void app_zdo_network_update_handler(zdo_network_update_notification_t *updateParam);

static void app_zdo_start_network_configuration(zdo_start_network_confirmation_t *confirmInfo);
static void app_zdo_leave_response(zdo_descriptor_response_t *zdpResp);
static void app_aps_data_handler(APS_DataInd_t *ind);

static void app_init_persistent_data_storage(void);

extern uint8_t send_packet ;

#ifdef _SECURITY_
static void app_init_security(void);
#endif

void APL_TaskHandler(void)
{
	switch(app_state)
	{

			case APP_INITING_STATE:
			{
				debug_init();
				app_initialization();
				app_state = APP_STARTING_NETWORK_STATE;
				app_post_global_task();
			}
			break;

			case APP_IN_NETWORK_STATE:
			{
				if(app_task_flags.app_sub_task_posted)
				{
					app_task_flags.app_sub_task_posted = false;
				
					if(device_interface.app_device_task_handler)
					device_interface.app_device_task_handler();
				}

				if(app_task_flags.app_message_sender_task_posted)
				{
					app_task_flags.app_message_sender_task_posted = false;
					app_message_sender();
				}

				if(app_task_flags.app_command_handler_task_posted)
				{
					app_task_flags.app_command_handler_task_posted = false;
					app_command_handler();
				}
			}
			break;

			case APP_STARTING_NETWORK_STATE:
			{
				debug_print("APP_STARTING_NETWORK_STATE\r\n");
				
			   uint16_t short_pan_id;
			   read_config_server_parameter(CS_NWK_PANID_ID,&short_pan_id);
			   debug_print("ShortPan : %04X\r\n",short_pan_id);

					    uint32_t channel_mask;
					    read_config_server_parameter(CS_CHANNEL_MASK_ID,&channel_mask);
					    debug_print("channel_mask : %04X\r\n",channel_mask);

					    uint64_t pan_id;
					    read_config_server_parameter(CS_EXT_PANID_ID,&pan_id);
					    debug_print("pan_id : %08x%08x\r\n",(uint32_t)(pan_id >>32) , pan_id);

					    uint64_t mac_id;
					    read_config_server_parameter(CS_UID_ID,&mac_id);
					    debug_print("mac_id : %08x%08x\r\n",(uint32_t)(mac_id >>32) , mac_id);

				#if defined(_ENABLE_PERSISTENT_SERVER_) && (APP_USE_PDS == 1)
				network_joincontrol_t network_join_control;
				device_type_t device_type;
				read_config_server_parameter(CS_DEVICE_TYPE_ID, &device_type);
				if((DEVICE_TYPE_COORDINATOR == device_type) && (restored_from_persistent_data_storage))
				{
					network_join_control.clearNeighborTable = false;
					network_join_control.secured            = true;
					network_join_control.discoverNetworks   = false;
					network_join_control.annce              = false;
					network_join_control.method             = NWK_JOIN_VIA_COMMISSIONING;
					write_config_server_parameter(CS_JOIN_CONTROL_ID, &network_join_control);
				}
				#endif
			
				zdo_start_request.ZDO_StartNetworkConf = app_zdo_start_network_configuration;
				ZDO_StartNetworkReq(&zdo_start_request);
			}
			break;

			case APP_LEAVING_NETWORK_STATE:
			{
				ZDO_MgmtLeaveReq_t *zdp_leave_request = &leave_request.req.reqPayload.mgmtLeaveReq;

				leave_request.ZDO_ZdpResp =  app_zdo_leave_response;
				leave_request.reqCluster = MGMT_LEAVE_CLID;
				leave_request.dstAddrMode = APS_EXT_ADDRESS;
				COPY_EXT_ADDR(leave_request.dstAddress.extAddress, *MAC_GetExtAddr());
				
				zdp_leave_request->deviceAddr = 0;
				zdp_leave_request->rejoin = 0;
				zdp_leave_request->removeChildren = 1;
				zdp_leave_request->reserved = 0;
				
				ZDO_ZdpReq(&leave_request);
			}
			break;

			default:
			break;
	}
}

static void app_initialization(void)
{
	ExtAddr_t uid;
	read_config_server_parameter(CS_UID_ID,&uid);
	if(uid == 0 || uid > APS_MAX_UNICAST_EXT_ADDRESS)
	{
		BSP_ReadUid(&uid);
		write_config_server_parameter(CS_UID_ID, &uid);
	}
	
	device_type_t device_type;

	#if APP_DEVICE_TYPE == DEV_TYPE_COORDINATOR
	app_start_usart_manager();
	app_get_coordinator_interface(&device_interface);
	#elif APP_DEVICE_TYPE == DEV_TYPE_ROUTER
	app_get_router_interface(&device_interface);
	#else
	app_get_end_device_interface(&device_interface);
	#endif

	app_init_persistent_data_storage();

	device_type = WSN_DEMO_DEVICE_TYPE;
	write_config_server_parameter(CS_DEVICE_TYPE_ID, &device_type);
	app_network_info.nodeType = device_type;

	simpleDescriptor.endpoint = WSNDEMO_ENDPOINT;
	simpleDescriptor.AppProfileId = CCPU_TO_LE16(WSNDEMO_profileId);
	simpleDescriptor.AppDeviceId = CCPU_TO_LE16(WSNDEMO_DEVICE_ID);
	simpleDescriptor.AppDeviceVersion = WSNDEMO_DEVICE_VERSION;
	end_point_params.simpleDescriptor = &simpleDescriptor;
	end_point_params.APS_DataInd = app_aps_data_handler;
	APS_RegisterEndpointReq(&end_point_params);

	#ifdef _SECURITY_
	app_init_security();
	#endif
	{
		ExtAddr_t extended_address;

		read_config_server_parameter(CS_UID_ID, &extended_address);
		app_network_info.extended_address = extended_address;
	}

	#if APP_USE_OTAU == 1
	appZclManagerInit();
	#endif

	if(device_interface.app_device_initialization)
	{
		device_interface.app_device_initialization();
	}
	
	app_init_message_sender();
	app_init_command_handler();
}

#ifdef _SECURITY_

static void app_init_security(void)
{
	ExtAddr_t extended_address;
	
	#if APP_DEVICE_TYPE == DEV_TYPE_COORDINATOR
	read_config_server_parameter(CS_UID_ID, &extended_address);
	write_config_server_parameter(CS_APS_TRUST_CENTER_ADDRESS_ID, &extended_address);
	#else
	extended_address = APS_UNIVERSAL_EXTENDED_ADDRESS;
	write_config_server_parameter(CS_APS_TRUST_CENTER_ADDRESS_ID, &extended_address);
	#endif

	#if APP_DEVICE_TYPE == DEV_TYPE_COORDINATOR
	uint8_t network_default_key[SECURITY_KEY_SIZE];

	read_config_server_parameter(CS_NETWORK_KEY_ID, &network_default_key);
	NWK_SetKey(network_default_key, NWK_STARTUP_ACTIVE_KEY_SEQUENCE_NUMBER);
	NWK_ActivateKey(NWK_STARTUP_ACTIVE_KEY_SEQUENCE_NUMBER);
	#endif

	#ifdef _LINK_SECURITY_
	
	#if defined(_ENABLE_PERSISTENT_SERVER_) && (APP_USE_PDS == 1)
	if(!restored_from_persistent_data_storage)
	#endif
	{
		uint8_t link_Key[16] = LINK_KEY;

		ExtAddr_t extended_address = APS_UNIVERSAL_EXTENDED_ADDRESS;
		aps_set_link_key(&extended_address, link_Key);
	}
	#endif
}
#endif

static void app_init_persistent_data_storage(void)
{
	#if defined(_ENABLE_PERSISTENT_SERVER_) && (APP_USE_PDS == 1)
	PDS_StoreByEvents(BC_ALL_MEMORY_MEM_ID);

	BSP_OpenButtons(NULL, NULL);

	if(BSP_ReadButtonsState() & 0x01)
	{
		PDS_DeleteAll(false);
	}
	else if(PDS_IsAbleToRestore(BC_ALL_MEMORY_MEM_ID))
	{
		restored_from_persistent_data_storage = true;
		PDS_Restore(BC_ALL_MEMORY_MEM_ID);
	}
	if(PDS_IsAbleToRestore(NWK_SECURITY_COUNTERS_ITEM_ID))
	{
		PDS_Restore(NWK_SECURITY_COUNTERS_ITEM_ID);
	}

	BSP_CloseButtons();
	#endif
}

static void app_zdo_leave_response(zdo_descriptor_response_t *zdpResp)
{
	if(ZDO_SUCCESS_STATUS == zdpResp->respPayload.status)
	{
		app_state = APP_STOP_STATE;
	}
	
	else
	{
		app_post_global_task();
	}
	
}

static void app_zdo_start_network_configuration(zdo_start_network_confirmation_t *startInfo)
{

	if(ZDO_SUCCESS_STATUS == startInfo->status)
	{
		send_packet = 1;
		app_state = APP_IN_NETWORK_STATE;
		
		#if APP_USE_OTAU == 1
		run_ota_upgrade_service();
		#endif
		
		if(device_interface.app_device_task_reset)
		{
			device_interface.app_device_task_reset();
		}
		
		app_network_info.panID                = startInfo->PANId;
		app_network_info.shortAddr            = startInfo->shortAddr;
		app_network_info.parentShortAddr = startInfo->parentAddr;
		app_network_info.workingChannel       = startInfo->activeChannel;
	}
	else
	{
		app_post_global_task();
	}
}

void zdo_network_update_notification_handler(zdo_network_update_notification_t *network_params)
{
	if(ZDO_NO_KEY_PAIR_DESCRIPTOR_STATUS == network_params->status)
	{
		#ifdef _LINK_SECURITY_
		ExtAddr_t addr         = network_params->childInfo.extended_address;
		uint8_t   link_Key[16] = LINK_KEY;
		aps_set_link_key(&addr, link_Key);
		#endif
	}
	else
	{
		app_zdo_network_update_handler(network_params);
	}
	
}

static void app_zdo_network_update_handler(zdo_network_update_notification_t *updateParam)
{
	switch(updateParam->status)
	{
			case ZDO_NETWORK_LOST_STATUS:
			app_state = APP_STOP_STATE;
			break;

			case ZDO_NETWORK_LEFT_STATUS:
			app_state = APP_STARTING_NETWORK_STATE;
			app_post_global_task();
			break;

			case ZDO_NWK_UPDATE_STATUS:
			case ZDO_NETWORK_STARTED_STATUS:
			app_network_info.shortAddr            = updateParam->nwkUpdateInf.shortAddr;
			app_network_info.panID                = updateParam->nwkUpdateInf.panId;
			app_network_info.parentShortAddr = updateParam->nwkUpdateInf.parentShortAddr;
			app_network_info.workingChannel       = updateParam->nwkUpdateInf.currentChannel;

			if(APP_STARTING_NETWORK_STATE == app_state)
			{
				#if APP_USE_OTAU == 1
				run_ota_upgrade_service();
				#endif
			
			
				if(device_interface.app_device_task_reset)
				{
					device_interface.app_device_task_reset();
				}
			
			}
		
			if(APP_STOP_STATE == app_state)
			{
				app_state = APP_IN_NETWORK_STATE;
			
				if(device_interface.app_device_task_reset)
				{
					device_interface.app_device_task_reset();
				}
			
				app_post_global_task();
			}
			app_state = APP_IN_NETWORK_STATE;
			break;

			default:
			break;
	}
}

void zdo_wakeup_indication(void)
{
}

void app_read_lqi_rssi(void)
{
	ZDO_GetLqiRssi_t lqi_rssi;

	lqi_rssi.nodeAddr = app_network_info.parentShortAddr;
	ZDO_GetLqiRssi(&lqi_rssi);

	app_network_info.lqi  = lqi_rssi.lqi;
	app_network_info.rssi = lqi_rssi.rssi;
}

static void app_post_global_task(void)
{
	sys_post_task(APL_TASK_ID);
}

void app_post_sub_task(void)
{
	if(APP_IN_NETWORK_STATE == app_state)
	{
		app_task_flags.app_sub_task_posted = true;
		sys_post_task(APL_TASK_ID);
	}
}

void app_post_command_handler_task(void)
{
	if(APP_IN_NETWORK_STATE == app_state)
	{
		app_task_flags.app_command_handler_task_posted = true;
		sys_post_task(APL_TASK_ID);
	}
}

void app_post_message_sender_task(void)
{
	if(APP_IN_NETWORK_STATE == app_state)
	{
		app_task_flags.app_message_sender_task_posted = true;
		sys_post_task(APL_TASK_ID);
	}
}

void app_leave_network(void)
{
	if(APP_IN_NETWORK_STATE == app_state)
	{
		app_state = APP_LEAVING_NETWORK_STATE;
		app_post_global_task();
	}
}

void app_command_received(void *in_command, uint8_t cmdSize)
{
	app_command_type *command = (app_command_type *)in_command;
	app_create_command(&command);

	(void)cmdSize;
}


bool app_get_command_descriptor(app_command_descriptor_t *command_descriptor, uint8_t command_id)
{
	if(device_interface.app_device_command_descriptor_table)
	{
		for(uint8_t i = 0; i < device_interface.app_device_command_descriptor_table_size; i++)
		{
			memcpy_P(command_descriptor, &device_interface.app_device_command_descriptor_table[i], sizeof(app_command_descriptor_t));

			if(command_id == command_descriptor->commandID)
			{
				return true;
			}
		}
	}

	memset(command_descriptor, 0, sizeof(app_command_descriptor_t));
	return false;
}

static void app_aps_data_handler(APS_DataInd_t *ind)
{
	// app_command_type *command = (app_command_type *)ind->asdu;
	app_command_type *command = (app_command_type *)ind;
	app_create_command(&command);
}

#ifdef _BINDING_
void ZDO_BindIndication(zdo_bind_indication_t *bindInd)
{
	(void)bindInd;
}

void ZDO_UnbindIndication(zdo_unbind_indication_t *unbindInd)
{
	(void)unbindInd;
}
#endif

#ifdef ZAPPSI_HOST
void ZSI_StatusUpdateNotf(ZSI_UpdateNotf_t *notf)
{
	(void *)notf;
}
#endif
