#ifdef _ENDDEVICE_


#include <sysUtils.h>
#include <sysQueue.h>
#include <app_apl_task.h>
#include <sysTaskManager.h>
#include <sysIdleHandler.h>
#include <zdo.h>
#include <configServer.h>
#include <aps.h>
#include <mac.h>
#include <app_command_handler.h>
#include <app_message_sender.h>
#include <app_end_device.h>
#include <bspUid.h>
#include <usart.h>
#include <debug.h>
#include <converted_library_types.h>


#define APP_TIMER_ENDDEVICE_SENDING_PERIOD         200UL

bool app_end_device_network_command_handler(app_command_type *command);

static PROGMEM_DECLARE(app_command_descriptor_t app_end_device_command_descriptor_table[]) =
{
	APP_COMMAND_DESCRIPTOR(APP_NETWORK_INFO_COMMAND_ID, app_end_device_network_command_handler),
};

extern app_network_command_payload_t app_network_info;
extern app_state_type app_state;

static void device_timerFired(void);
static void app_end_device_task_handler(void);
static void app_end_device_init(void);
static void app_end_device_task_reset(void);

static DeviceState_t  app_device_state = INITIAL_DEVICE_STATE;
uint32_t packet_count = 0;
hal_application_timer_t device_timer;

uint8_t send_packet = 1;

static void app_end_device_task_handler(void)
{
	switch (app_device_state)
	{
			case READING_SENSORS_STATE:
			{
				app_read_lqi_rssi();
				app_device_state = SENDING_DEVICE_STATE;
				app_post_sub_task();
			}
			break;

			case SENDING_DEVICE_STATE:
			{
				if(send_packet == 1)
				{
					send_packet = 0;
					debug_print("LQI::%d, RSSI :%d , packet_count::%d\r\n",	app_network_info.lqi, app_network_info.rssi ,++packet_count);
					app_command_type *command = NULL;

					if (app_create_command(&command))
					{
						command->id = APP_NETWORK_INFO_COMMAND_ID;
						memcpy(&command->payload.network_information, &app_network_info, sizeof(app_network_command_payload_t));
					}
					
				}

				app_device_state = STARTING_TIMER_STATE;
				app_post_sub_task();
			}
			break;

			case STARTING_TIMER_STATE:
			{
				hal_start_app_timer(&device_timer);
			}
			break;

			case INITIAL_DEVICE_STATE:
			{
				hal_stop_app_timer(&device_timer);
				device_timer.interval = APP_TIMER_ENDDEVICE_SENDING_PERIOD;
				device_timer.mode     = TIMER_ONE_SHOT_MODE;
				device_timer.callback = device_timerFired;

				app_device_state = READING_SENSORS_STATE;
				app_post_sub_task();

				/*#if defined(_SLEEP_WHEN_IDLE_)
					SYS_EnableSleepWhenIdle();
				#endif*/
			}
			break;

			default:
			break;
	}
}

static void app_end_device_init(void)
{
	bool rxOnWhenIdleFlag = false;

	write_config_server_parameter(CS_RX_ON_WHEN_IDLE_ID, &rxOnWhenIdleFlag);
	app_device_state = INITIAL_DEVICE_STATE;
}

static void app_end_device_task_reset(void)
{
	app_device_state = INITIAL_DEVICE_STATE;
	app_post_sub_task();
}

static void device_timerFired(void)
{
	app_device_state = READING_SENSORS_STATE;
	app_post_sub_task();
}

bool app_end_device_network_command_handler(app_command_type *command)
{
	APS_DataReq_t *pMsgParams = NULL;

	if (app_create_transmission_frame(&pMsgParams, &command, NULL))
	{
		memset(pMsgParams, 0, sizeof(APS_DataReq_t));

		pMsgParams->profileId                            = CCPU_TO_LE16(WSNDEMO_profileId);
		pMsgParams->dstAddrMode              = APS_SHORT_ADDRESS;
		pMsgParams->dstAddress.shortAddress      = CPU_TO_LE16(0);
		pMsgParams->dstEndpoint                 = 1;
		pMsgParams->clusterId                            = CPU_TO_LE16(1);
		pMsgParams->srcEndpoint                      = WSNDEMO_ENDPOINT;
		pMsgParams->asduLength                           = sizeof(app_network_command_payload_t) + sizeof(command->id);
		pMsgParams->txOptions.acknowledgedTransmission = 1;
		
		#ifdef _APS_FRAGMENTATION_
		   pMsgParams->txOptions.fragmentationPermitted = 1;
		#endif
		
		#ifdef _LINK_SECURITY_
		   pMsgParams->txOptions.securityEnabledTransmission = 1;
		#endif
		
		pMsgParams->radius = 0x0;
	}

	return true;
}


void app_get_end_device_interface(get_devicetype_interface_t *device_interface)
{
	device_interface->app_device_command_descriptor_table      = app_end_device_command_descriptor_table;
	device_interface->app_device_command_descriptor_table_size = sizeof(app_end_device_command_descriptor_table);
	device_interface->app_device_initialization                = app_end_device_init;
	device_interface->app_device_task_handler                  = app_end_device_task_handler;
	device_interface->app_device_task_reset                    = app_end_device_task_reset;
}

#endif

