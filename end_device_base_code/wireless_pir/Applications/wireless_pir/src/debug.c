#include <stdio.h>
#include <string.h>
#include <debug.h>
#include <converted_library_types.h>
#include <configuration.h>

bool message_sending_in_progress;

#define DEBUG_MODE

#ifdef DEBUG_MODE
uint16_t head, tail, new_head;
HAL_UsartDescriptor_t debug_usart_descriptor;
uint8_t debug_buffer[DBG_BUFFER_MAX_SIZE], temp_buffer[DBG_MAX_MSG_SIZE];
uint8_t rx_buffer[1];
#endif

#ifdef DEBUG_MODE
static void uart_received_byte_event(uint16_t readBytesLen);
static void write_debug_message_confirmation(void);

static void send_next_debug_message(void)
{
	if(tail == new_head)
	{
		tail = head = new_head = 0;
		message_sending_in_progress = false;
	}
	else if(new_head < tail)
	{
		head= 0;
		hal_usart_write(&debug_usart_descriptor, &debug_buffer[tail], (DBG_BUFFER_MAX_SIZE - tail));
	}
	else
	{
		head= new_head;
		hal_usart_write(&debug_usart_descriptor, &debug_buffer[tail], (head - tail));
	}
}

static void write_debug_message_confirmation(void)
{
	tail= head;
	send_next_debug_message();
}

static void uart_received_byte_event(uint16_t bytesReceived)
{
	bytesReceived--;
}
#endif

void debug_init()
{
	message_sending_in_progress = false;

	#ifdef DEBUG_MODE
		debug_usart_descriptor.tty = USART_CHANNEL_0;
		debug_usart_descriptor.mode = USART_MODE_ASYNC;
		debug_usart_descriptor.flowControl = USART_FLOW_CONTROL_NONE;
		debug_usart_descriptor.baudrate = USART_BAUDRATE_115200;
		debug_usart_descriptor.dataLength = USART_DATA8;
		debug_usart_descriptor.parity = USART_PARITY_NONE;
		debug_usart_descriptor.stopbits = USART_STOPBIT_1;
		debug_usart_descriptor.rxBuffer = rx_buffer;
		debug_usart_descriptor.rxBufferLength = 1;
		debug_usart_descriptor.txBuffer = NULL;
		debug_usart_descriptor.txBufferLength = 0;
		debug_usart_descriptor.rxCallback = uart_received_byte_event;
		debug_usart_descriptor.txCallback = write_debug_message_confirmation;

		head = new_head = tail = 0;
		hal_usart_open(&debug_usart_descriptor);
	#endif
}

int8_t debug_print(char *format,...)
{
	#ifdef DEBUG_MODE
	uint16_t size;
	va_list var;

	va_start(var,format);
	size = vsprintf((char *)temp_buffer, format, var);
	va_end(var);

	if((tail == new_head) && (new_head == head))
	{
		memcpy(debug_buffer, temp_buffer, size);
		new_head = size;
		tail = 0;
	}
	else if(new_head < tail)
	{
		if((new_head - tail + size) < DBG_BUFFER_MAX_SIZE)
		{
			memcpy(&debug_buffer[new_head], temp_buffer, size);
			new_head += size;
		}
		else
		{
			return -1;
		}
	}
	else
	{
		if((new_head + size) < DBG_BUFFER_MAX_SIZE)
		{
			memcpy(&debug_buffer[new_head], temp_buffer, size);
			new_head += size;
		}
		else if((new_head - tail + size) < DBG_BUFFER_MAX_SIZE)
		{
			uint16_t len = DBG_BUFFER_MAX_SIZE - new_head;
			memcpy(&debug_buffer[new_head], temp_buffer, len);
			memcpy(&debug_buffer, &temp_buffer[len], (size - len));
			new_head += size - DBG_BUFFER_MAX_SIZE;
		}
		else
		{
			return -1;
		}
	}

	if(message_sending_in_progress == false)
	{
		message_sending_in_progress = true;
		send_next_debug_message();
	}
	#else
	message_sending_in_progress = false;
	(void *)(format);
	#endif
	return 0;
}
