#include <sysUtils.h>
#include <sysQueue.h>
#include <app_apl_task.h>
#include <sysTaskManager.h>
#include <zdo.h>
#include <configServer.h>
#include <aps.h>
#include <mac.h>
#include <app_command_handler.h>
#include <app_message_sender.h>
#include <app_end_device.h>
#include <bspUid.h>
#include <usart.h>
#include <debug.h>
#include <converted_library_types.h>

typedef struct _app_command_queue_element_t
{
  queue_element_t next;
  app_command_type in_command;

} app_command_queue_element_t;

static app_command_type *app_get_command_to_process(void);
static app_command_queue_element_t  app_command_buffer[MAX_CMD_QUEUE_COUNT];
static DECLARE_QUEUE(app_busy_command_queue);
static DECLARE_QUEUE(app_free_command_queue);

void app_init_command_handler(void)
{
  reset_queue(&app_busy_command_queue);
  reset_queue(&app_free_command_queue);

  for(uint8_t i = 0; i < ARRAY_SIZE(app_command_buffer); i++)
  {
    if(!put_element_into_queue(&app_free_command_queue, &app_command_buffer[i].next))
    {
      /* failed to queue */
    }
  }
}

void app_command_handler(void)
{
  app_command_type *current_command = app_get_command_to_process();

  if(current_command)
  {
    bool delete_command = true;
    app_command_descriptor_t command_descriptor;

    if(app_get_command_descriptor(&command_descriptor, current_command->id))
    {
      delete_command = command_descriptor.serviceVector(current_command);
    }

    if(delete_command)
    {
      if(true != put_element_into_queue(&app_free_command_queue, delete_head_element_in_queue(&app_busy_command_queue)))
      {
        /* failed to queue */
      }
    }
    else
    {
      if(true != put_element_into_queue(&app_busy_command_queue, delete_head_element_in_queue(&app_busy_command_queue)))
      {
        /* failed to queue */
      }
    }

    app_post_command_handler_task();
  }
}

bool is_command_queue_free(void)
{
  return app_get_command_to_process() ? false : true;
}

static app_command_type *app_get_command_to_process(void)
{
  queue_element_t *queueElement = get_queue_element(&app_busy_command_queue);

    if(queueElement)
    {
       return &(GET_STRUCT_BY_FIELD_POINTER(app_command_queue_element_t,
                                         next,
                                         queueElement))->in_command;
    }

  return NULL;
}

bool app_create_command(app_command_type **command_pointer)
{
  app_command_queue_element_t *app_command_queue_element = NULL;
  queue_element_t *free_element = NULL;
  bool result = false;

  sys_assert(command_pointer, CMDQUEUEINSERTASSERT_0);

  free_element = get_queue_element(&app_free_command_queue);

  if(free_element)
  {
    if(!put_element_into_queue(&app_busy_command_queue, delete_head_element_in_queue(&app_free_command_queue)))
    {
      /* failed to queue */
    }
    app_command_queue_element = GET_STRUCT_BY_FIELD_POINTER(app_command_queue_element_t,
                                                     next,
                                                     free_element);

    if(*command_pointer)
    {
      memcpy(&app_command_queue_element->in_command, *command_pointer, sizeof(app_command_type));
    }

    *command_pointer = &app_command_queue_element->in_command;

    result = true;
  }

  app_post_command_handler_task();
  return result;
}

