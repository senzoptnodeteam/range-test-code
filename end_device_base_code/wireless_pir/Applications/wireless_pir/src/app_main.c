#include <sysUtils.h>
#include <sysQueue.h>
#include <app_apl_task.h>
#include <sysTaskManager.h>
#include <zdo.h>
#include <configServer.h>
#include <aps.h>
#include <mac.h>
#include <app_command_handler.h>
#include <app_message_sender.h>
#include <app_router.h>
#include <app_end_device.h>
#include <bspUid.h>
#include <usart.h>
#include <debug.h>
#include <converted_library_types.h>



int main(void)
{
	
	sys_init();
	
	for(;;)
	{
		sys_run_tasks();
	}
}


