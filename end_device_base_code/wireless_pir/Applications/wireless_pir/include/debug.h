#ifndef _SZ_DBG_LIB_H
#define _SZ_DBG_LIB_H

#include <usart.h>
#include <stdarg.h>

#define DBG_MAX_MSG_SIZE				   200
#define DBG_BUFFER_MAX_SIZE			   1024

void debug_init(void);
int8_t debug_print(char *format,...);

#endif