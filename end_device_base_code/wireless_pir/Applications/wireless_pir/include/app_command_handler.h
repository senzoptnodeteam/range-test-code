#ifndef _WSNCOMMANDHANDLER_H
#define _WSNCOMMANDHANDLER_H

#include <app_commands.h>

void app_init_command_handler(void);
void app_command_handler(void);
bool app_create_command(app_command_type **command_pointer);
bool is_command_queue_free(void);

#endif
