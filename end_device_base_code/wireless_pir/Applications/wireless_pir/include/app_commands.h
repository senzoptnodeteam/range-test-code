#ifndef _WSNCOMMAND_H
#define _WSNCOMMAND_H

#include <macAddr.h>
#include <aps.h>

#define APP_NETWORK_INFO_COMMAND_ID    0x01
#define APP_IDENTIFY_COMMAND_ID        0x10
#define APP_IDENTIFY_NOTF_COMMAND_ID   0x11

#if APP_USE_DEVICE_CAPTION == 1
#define APP_MAX_DEVICE_CAPTION_SIZE   16U
#define APP_DEVICE_CAPTION_SIZE       (sizeof(APP_DEVICE_CAPTION) - 1) 
#else
#define APP_MAX_DEVICE_CAPTION_SIZE   0U
#define APP_DEVICE_CAPTION_SIZE       0U
#endif 

#define APP_COMMAND_DESCRIPTOR(ID, SERV_VECT) {.commandID = ID, .serviceVector = SERV_VECT}

BEGIN_PACK

#if APP_USE_DEVICE_CAPTION == 1
typedef struct _app_device_caption_t
{
  uint8_t fieldType;
  uint8_t size;
  char caption[APP_DEVICE_CAPTION_SIZE];
} PACK app_device_caption_t;
#endif

typedef struct PACK _app_board_info_t
{
  uint8_t     boardType;//1
  uint8_t     sensorsSize;//1
  struct
  {
    int32_t  battery;
    int32_t  temperature;
    int32_t  light;
  } meshbean;
} PACK app_board_info_t;


typedef struct PACK _app_network_command_payload_t
{
  uint8_t        nodeType;
  ExtAddr_t      extended_address;
  ShortAddr_t    shortAddr;
  uint32_t       softVersion;
  uint32_t       channelMask;
  PanId_t        panID;
  Channel_t      workingChannel;
  ShortAddr_t    parentShortAddr;
  uint8_t        lqi;
  int8_t         rssi;
  app_board_info_t boardInfo;
#if APP_USE_DEVICE_CAPTION == 1
  app_device_caption_t deviceCaption;
#endif
} PACK app_network_command_payload_t;


typedef struct PACK _app_identify_request_payload_t
{
  ExtAddr_t     dstAddress;
  uint16_t      blink_duration_in_ms;
  uint16_t      blink_period_ms;
} app_identify_request_payload_t;


typedef struct PACK _app_identify_notification_payload_t
{
  uint8_t       status;
  ExtAddr_t     source_address;
} app_identify_notification_payload_t;

typedef struct PACK _AppCommand_t
{
  uint8_t                    id;
  union
  {
    app_network_command_payload_t           network_information;
    app_identify_request_payload_t          identify;
    app_identify_notification_payload_t         identifyNotf;
  } payload;
} PACK app_command_type;


typedef struct PACK _app_command_frame_t
{
  uint8_t      header[APS_ASDU_OFFSET];
  app_command_type in_command;
  uint8_t      footer[APS_AFFIX_LENGTH - APS_ASDU_OFFSET];
} PACK app_command_frame_t;

END_PACK



typedef bool (*command_service_vector_t)(app_command_type *command);


typedef struct _app_command_descriptor_t
{
  uint8_t                  commandID;     
  command_service_vector_t   serviceVector; 
} app_command_descriptor_t;

#endif 
