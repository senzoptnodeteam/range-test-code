#ifndef _WSNDEMOAPP_H
#define _WSNDEMOAPP_H

#include <configuration.h>
#include <macAddr.h>
#include <appFramework.h>
#include <configServer.h>
#include <appTimer.h>
#include <aps.h>
#include <zdo.h>
#include <dbg.h>
#include <leds.h>
#include <buttons.h>
#include <app_commands.h>


#if APP_DEVICE_TYPE == DEV_TYPE_COORDINATOR && defined(_COORDINATOR_)
  #define WSN_DEMO_DEVICE_TYPE   DEVICE_TYPE_COORDINATOR
#elif APP_DEVICE_TYPE == DEV_TYPE_ROUTER && defined(_ROUTER_)
  #define WSN_DEMO_DEVICE_TYPE   DEVICE_TYPE_ROUTER
#elif APP_DEVICE_TYPE == DEV_TYPE_ENDDEVICE && defined(_ENDDEVICE_)
  #define WSN_DEMO_DEVICE_TYPE   DEVICE_TYPE_END_DEVICE
#else
  #error Chosen application device type does not correspond to the library type.
#endif

#define WSNDEMO_profileId            1U
#define WSNDEMO_DEVICE_ID             1U
#define WSNDEMO_DEVICE_VERSION        1U
#define WSNDEMO_ENDPOINT              1U
#define MESHBEAN_SENSORS_TYPE         1U
#define DEVICE_CAPTION_FIELD_TYPE     32U


#define LINK_KEY {0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa}

#define MAX_USART_MESSAGE_QUEUE_COUNT 1U
#define MAX_CMD_QUEUE_COUNT           4U
#define MAX_APP_MSG_QUEUE_COUNT       2U

#define APP_MAX_PAYLOAD               (sizeof(app_command_type) - APP_DEVICE_CAPTION_SIZE + APP_MAX_DEVICE_CAPTION_SIZE)

#define MAX_RAW_APP_MESSAGE_SIZE  (2 + 2 * APP_MAX_PAYLOAD + 2 + 1)

#ifndef APP_TIMER_SENDING_PERIOD
#define APP_TIMER_SENDING_PERIOD         1000UL
#endif


#define BSPRESDSENSORDATAASSERT_0  0xf001
#define BSPRESDSENSORDATAASSERT_1  0xf002
#define BSPRESDSENSORDATAASSERT_2  0xf003
#define BSPRESDSENSORDATAASSERT_3  0xf004
#define BSPRESDSENSORDATAASSERT_4  0xf005
#define BSPRESDSENSORDATAASSERT_5  0xf006
#define BSPRESDSENSORDATAASSERT_6  0xf007
#define BSPRESDSENSORDATAASSERT_7  0xf008
#define BSPRESDSENSORDATAASSERT_8  0xf009

#define CMDQUEUEINSERTASSERT_0     0xf020
#define TXQUEUEINSERTASSERT_0      0xf021

typedef enum
{
  APP_INITING_STATE,
  APP_STARTING_NETWORK_STATE,
  APP_IN_NETWORK_STATE,
  APP_LEAVING_NETWORK_STATE,
  APP_STOP_STATE
} app_state_type;

typedef enum
{
  INITIAL_DEVICE_STATE,
  SENDING_DEVICE_STATE,
  READING_SENSORS_STATE,
  STARTING_TIMER_STATE,
} DeviceState_t;

typedef struct _get_devicetype_interface_t
{
  void (*app_device_task_handler)(void);
  void (*app_device_initialization)(void);
  void (*app_device_task_reset)(void);
  const app_command_descriptor_t FLASH_PTR *app_device_command_descriptor_table;
  uint8_t app_device_command_descriptor_table_size;
} get_devicetype_interface_t;


void app_post_sub_task(void);
void app_post_command_handler_task(void);
void app_post_message_sender_task(void);
void app_leave_network(void);
void app_command_received(void *in_command, uint8_t cmdSize);
void app_start_identify_visualization(uint16_t duration, uint16_t period);
void appReadLqiRssi(void);
bool app_get_command_descriptor(app_command_descriptor_t *command_descriptor, uint8_t command_id);

#endif 

