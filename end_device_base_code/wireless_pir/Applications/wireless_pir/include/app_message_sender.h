#ifndef _WSNMESSAGESENDER_H
#define _WSNMESSAGESENDER_H

#include <app_apl_task.h>

typedef struct _app_command_handler_tranmission_frame_t
{
  app_command_frame_t command_frame;
  APS_DataReq_t msgParams;
} app_command_handler_tranmission_frame_t;


void app_init_message_sender(void);
void app_message_sender(void);
bool app_create_transmission_frame(APS_DataReq_t **pMsgParamsPtr, app_command_type **command_pointer, 
                     void (*onTxFinishedCb)(app_command_handler_tranmission_frame_t *tramsission_frame));

bool is_tx_queue_free(void);

#endif 
