#ifndef CONVERTED_LIBRARY_APIS_H_
#define CONVERTED_LIBRARY_APIS_H_

// --------------------- Bitcloud stack data types -------------------------
#define aps_data_indication_t                       APS_DataInd_t
//#define APS_DataConf_t   						 APS_DataConf_t
//#define APS_DataReq_t						    		 APS_DataReq_t
#define hal_application_timer_t                 				 HAL_AppTimer_t
#define zdo_network_update_notification_t           ZDO_MgmtNwkUpdateNotf_t
#define zdo_start_network_confirmation_t            ZDO_StartNetworkConf_t
#define zdo_descriptor_response_t                   ZDO_ZdpResp_t
#define zdo_bind_indication_t                       ZDO_BindInd_t
#define zdo_unbind_indication_t                     ZDO_UnbindInd_t


// --------------------- Bitcloud stack function ---------------------------
#define aps_data_request                            APS_DataReq
#define aps_set_link_key                            APS_SetLinkKey
#define bsp_init_leds													      BSP_OpenLeds
#define bsp_deinit_leds													    BSP_CloseLeds
#define bsp_turn_on_led														  BSP_OnLed
#define bsp_turn_off_led														BSP_OffLed
#define bsp_toggle_led														  BSP_ToggleLed
#define write_config_server_parameter                          CS_WriteParameter
#define read_config_server_parameter                           CS_ReadParameter
#define hal_reset_watchdog_timer                    HAL_ResetWdt
#define hal_start_watchdog_timer                    HAL_StartWdt
#define hal_start_app_timer											    HAL_StartAppTimer
#define hal_stop_app_timer                          HAL_StopAppTimer
#define hal_i2c_open_packet                         HAL_OpenI2cPacket
#define hal_i2c_write_packet								 HAL_WriteI2cPacket
#define hal_i2c_read_packet											    HAL_ReadI2cPacket
#define hal_i2c_send_stop                           HAL_SendI2CStop
#define hal_warm_reset                              HAL_WarmReset
#define hal_read_reset_reason								 HAL_ReadResetReason
#define hal_usart_write										HAL_WriteUsart
#define hal_usart_open                              HAL_OpenUsart
#define hal_usart_close										 HAL_CloseUsart
#define network_get_next_hop								 NWK_GetNextHop
#define sys_post_task										 SYS_PostTask
#define sys_init												 SYS_SysInit
#define sys_run_tasks										 SYS_RunTask
#define sys_assert                                  sysAssert
#define zdo_get_parent_address							 ZDO_GetParentAddr
#define zdo_network_update_notification_handler     ZDO_MgmtNwkUpdateNotf
#define zdo_wakeup_indication                       ZDO_WakeUpInd

// ==========================================================================
// --------------------- Application data types -----------------------------
#define app_tx_queue_element_t		    							Apptx_queue_element_t
#define device_type_t														    DeviceType_t
#define queue_element_t												      QueueElement_t
#define queue_descriptor_t    											QueueDescriptor_t

// --------------------- Application functions ------------------------------
#define reset_queue                                 resetQueue
#define put_element_into_queue                      putQueueElem
#define delete_head_element_in_queue					 deleteHeadQueueElem
#define get_queue_element									 getQueueElem
#define app_read_lqi_rssi								    appReadLqiRssi
#define run_ota_upgrade_service                     runOtauService

#endif // CONVERTED_LIBRARY_APIS_H_
